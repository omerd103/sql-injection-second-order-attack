# Omer Dangoor Second order attack

An example of user registration and user login application developed using Node.js, Express.js and MySQL.

The application allows users to register, login and access to the user's private page session information.

We developed this project to simulate sql injection second order attack.

## Install

Make sure you have [Node.js](http://nodejs.org/) and [MySQL](http://www.mysql.com/) installed Or use our Docker-compose.yml to Run mySQL.

```sh
git clone https://gitlab.com/omerd103/sql-injection-second-order-attack.git
cd sql-injection-second-order-attack
npm install
```

## 1.Import

Upload `sql-file.sql` to MySQL


## 2.Start

```sh
node ./bin/www
```

Your app should now be running on [localhost:3000](http://localhost:3000/).


## Let the hacking Begin!!

### Go to register page

![1](/uploads/77717b5d884454844dfa2fea59118a89/1.jpg)

### Create the user you want to hack

`
user: Admin
password: 1234`
![2](/uploads/cfeb38012f34f012f3243a62bb5fb4b3/2.jpg)
 
 **on this stage we will crate the user you want to hack.**

### Create another user , it will be the target user with `'#` at the end

`
user: Admin'#
password: 123456`
![3](/uploads/45322d6fd2de6b5a52299da02b906860/3.jpg)

**now after we create the first user , we will create a new user with username as “admin'# –” and password “123456”. Note that, the username value “admin'# –” is actually the sql injection payload**

### Login to the second order user

`
user: Admin'#
password: 123456`

and click on "Change!" to the the password 
![4](/uploads/c313001137fc7410dd17191cbd5cfec2/4.jpg)

**So basically so far we have two usernames registered. “admin” and “admin'# –” using different passwords. After registering the account, I logged in using the username “admin'# –“.**

### Change the second order password 

**As the page offers password changed functionality, we  changed the password from “123456” to “12”.  Note that the username is “admin'# –” so lets construct the query for password change for this username.**

UPDATE users  
SET password='12'
WHERE username='admin'#--' and password='abc'

![5](/uploads/a4f03db42d0cf0068fbcea4d7162e5fc/5.jpg)

**The Query results in updating the password for the user “admin”, instead of “admin'# "hence successfully performing second order sql injection. Second Order SQL injection works in other scenarios as well where data from database is meant to retrieved, Hopefully it explains the concept.**

### Now you can login to the target user with the new password
![6](/uploads/97a24fc041f98e63b5b7bc0c849621e5/6.jpg)

### You now login to the traget account, You hacked the user!!!
![7](/uploads/fcdce191561431ca32153106c032220e/7.jpg)

well done if you got this far :)